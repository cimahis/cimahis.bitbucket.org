var searchData=
[
  ['c',['c',['../structContorno.html#abbe1844bbb792aa6e9e2bb2c845cc181',1,'Contorno::c()'],['../classInterfazForma.html#a64a128a45ec3a791476dd39b0b155852',1,'InterfazForma::c()']]],
  ['cajaaviso',['cajaAviso',['../classelegirFiltros.html#a6d371db217fbb305b50f0d6bcec48b82',1,'elegirFiltros']]],
  ['circularidad',['circularidad',['../structnp__s.html#aeecdfc4906b862f79698df639deeeb8e',1,'np_s::circularidad()'],['../structparametros.html#a0d27844f2e5f75554a067b99aa46cb9f',1,'parametros::circularidad()']]],
  ['codgop',['codgOp',['../structSgmUtils_1_1MorphOpParams.html#a75ab8f7e53f51b6fa195ecc28f1d7357',1,'SgmUtils::MorphOpParams']]],
  ['column',['column',['../structNodo.html#a51610152393e05b6293748d025546a9b',1,'Nodo']]],
  ['comboampliacion',['comboAmpliacion',['../classCimahis.html#a6cc4e158d54240c1e2b4e04fa5d60274',1,'Cimahis']]],
  ['configurarwavelet',['configurarWavelet',['../classCimahis.html#a006973b58683bdcfa6096731ed642cfc',1,'Cimahis']]],
  ['contornoactual',['contornoActual',['../classInterfazForma.html#af5d7446b4c3f9aa403b90a8c8384fc2e',1,'InterfazForma']]],
  ['contornos',['contornos',['../classInterfazForma.html#a02a0355378b243a870d1a31fa38a01bc',1,'InterfazForma']]],
  ['contornostrazados',['contornosTrazados',['../classInterfazForma.html#ab1a4b0a9ee73a1c9ae555514d1e2d242',1,'InterfazForma']]]
];
