var searchData=
[
  ['closing',['CLOSING',['../namespaceMorphLib.html#acdb16937c6eec6f3f77dd09537b92f3ba18ba7778aa8f573528fad5b86e432236',1,'MorphLib']]],
  ['closing_5fby_5freconst',['CLOSING_BY_RECONST',['../namespaceMorphLib.html#acdb16937c6eec6f3f77dd09537b92f3ba2f1c9c031f16dc25a5eece5636d629c2',1,'MorphLib']]],
  ['contours',['CONTOURS',['../classSgmGraphPixmap.html#a5dada35c49a03c7cffd3976e798e2bc3a05c9ca2a8a7ab5434209e93f3c4b748c',1,'SgmGraphPixmap']]],
  ['contours_5f4n',['CONTOURS_4N',['../namespaceMorphLib.html#acdb16937c6eec6f3f77dd09537b92f3ba0d8e4d6958dcbb8554ee8564bd1e522d',1,'MorphLib']]],
  ['contours_5f8n',['CONTOURS_8N',['../namespaceMorphLib.html#acdb16937c6eec6f3f77dd09537b92f3ba9c7cbc36c025cd7402e46f4bfb500266',1,'MorphLib']]],
  ['cross',['CROSS',['../classMorphSettingsDialog.html#a56e46203873629b55d344aaff5017432a304726751e526e21e2e30ea4142d43a8',1,'MorphSettingsDialog']]]
];
