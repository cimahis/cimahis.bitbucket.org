var searchData=
[
  ['abrir',['abrir',['../classCimahis.html#ab97d69e50972814a02b2bd19abf8fd4e',1,'Cimahis']]],
  ['abrirconfiguracionwavelet',['abrirConfiguracionWavelet',['../classCimahis.html#aab79512b8429e4835d7778b40475b041',1,'Cimahis']]],
  ['abrirgrafica',['abrirGrafica',['../classCimahis.html#a74d864cb4628769a8f4629570d146544',1,'Cimahis']]],
  ['abrirgraficacircularidad',['abrirGraficaCircularidad',['../classCimahis.html#ac4863d731a609f5d7dc19377b25283cc',1,'Cimahis']]],
  ['abrirgraficadiametro',['abrirGraficaDiametro',['../classCimahis.html#a1f370992fe4e004097a2d450c4211faa',1,'Cimahis']]],
  ['abrirgraficaejemayor',['abrirGraficaEjeMayor',['../classCimahis.html#ad9e3f927942853f2fd1f3f682d6051e7',1,'Cimahis']]],
  ['abrirgraficaejemenor',['abrirGraficaEjeMenor',['../classCimahis.html#a1b14cc014e8f06332c07613d823a2ba7',1,'Cimahis']]],
  ['abrirgraficafactorforma',['abrirGraficaFactorForma',['../classCimahis.html#ae283224678469acec41ae6e54f987fe3',1,'Cimahis']]],
  ['abrirgraficaicn',['abrirGraficaICN',['../classCimahis.html#a132ba16b21526bdec2c129eb41301626',1,'Cimahis']]],
  ['abrirgraficaperimetro',['abrirGraficaPerimetro',['../classCimahis.html#a3bf8205da800d0b6e4e07afbf320a43e',1,'Cimahis']]],
  ['abrirgraficarelacionab',['abrirGraficaRelacionAb',['../classCimahis.html#a067303396ed8415e3f3e1f015b4a3ed2',1,'Cimahis']]],
  ['abrirgraficatamano',['abrirGraficaTamano',['../classCimahis.html#aaad620d4ddd4b6271599597c5c13d470',1,'Cimahis']]],
  ['abrirurl',['abrirUrl',['../classPluginManager.html#aad19482c336ab996f5807caa428105bb',1,'PluginManager']]],
  ['abrirventanaconfig',['abrirVentanaConfig',['../classCimahis.html#a258db78269ff46e6ad6561c3ca9b1e8f',1,'Cimahis']]],
  ['abrirventanacontornos',['abrirVentanaContornos',['../classCimahis.html#afa27c594296b1af7be7d44bb55c81d9c',1,'Cimahis']]],
  ['abrirventanafiltros',['abrirVentanaFiltros',['../classCimahis.html#a577c84f6eaf13ca5e83b25727064aebe',1,'Cimahis']]],
  ['accionborrarenqlistwidget',['accionBorrarEnQListWidget',['../classInterfazForma.html#a1b8f4e41a5c905809340d0c135784328',1,'InterfazForma']]],
  ['acercade',['acercaDe',['../classCimahis.html#a2d0eeb0f2dfbfd7d42314e6d82e25a26',1,'Cimahis']]],
  ['activarguardar',['activarGuardar',['../classConfigurarWavelets.html#a1b5ab61227fe39347a663c75e3bf211b',1,'ConfigurarWavelets']]],
  ['actualizardatosestadisticos',['actualizarDatosEstadisticos',['../classVisualizarHistograma.html#a59893bfeb4ead2c127e8ae3db6d63d46',1,'VisualizarHistograma']]],
  ['actualizarhabilitadodeshabilitado',['actualizarHabilitadoDeshabilitado',['../classPluginManager.html#a5b27e550060131f5f63189af1fda1a2d',1,'PluginManager']]],
  ['actualizarvistaprevia',['actualizarVistaPrevia',['../classelegirFiltros.html#abb7d794a8d76a1ee56d97c7c7518a06b',1,'elegirFiltros']]],
  ['actulizarx',['actulizarX',['../classlienzoHistograma.html#a5c4c6ad696974e0b419c0caf9e2deb52',1,'lienzoHistograma']]],
  ['addbox',['addBox',['../classSgmGraphPixmap.html#a024ceda8be731d242ba1078793829589',1,'SgmGraphPixmap']]],
  ['addcontour',['addContour',['../classSgmGraphPixmap.html#aefcf548fb802f072b9c65e089fa690af',1,'SgmGraphPixmap']]],
  ['addmarker',['addMarker',['../classSgmGraphPixmap.html#a4548908ca71aba16eed91307df0c7706',1,'SgmGraphPixmap']]],
  ['adjustrowscolsspinboxs',['adjustRowsColsSpinBoxs',['../classMorphSettingsDialog.html#a9cb776761372ccc8e48bf79b8a3c4c03',1,'MorphSettingsDialog']]],
  ['agregaraccion',['agregarAccion',['../classCimahis.html#a58b43524dc6182b35412cca920ae2974',1,'Cimahis::agregarAccion()'],['../classCPluginData.html#a1faa2fad240c6451b6c565ae7e96af39',1,'CPluginData::agregarAccion()'],['../classPluginHandler.html#af32ae95d713d32e6e98d96130b2c314f',1,'PluginHandler::agregarAccion()']]],
  ['agregarmetadatos',['agregarMetadatos',['../classPluginManager.html#aaa1741cbbb246ceff0e85ae40cd8d486',1,'PluginManager']]],
  ['agregarpluginalista',['agregarPluginALista',['../classPluginManager.html#adeeb667ff5e68ddfd2edc9d69b92d174',1,'PluginManager::agregarPluginALista(QTreeWidgetItem *, const char *)'],['../classPluginManager.html#a30677204acbcb1b493f26bace36912f0',1,'PluginManager::agregarPluginALista(QObject *plugin, const QString &amp;text)']]],
  ['allowsdontcarevalues',['allowsDontCareValues',['../namespaceMorphLib.html#a65c7b80c0b2ca7e5673ed12709594e51',1,'MorphLib']]],
  ['ampliar',['ampliar',['../classCimahis.html#a354439ab87c365b68ac2a07a20b88a0b',1,'Cimahis']]],
  ['ampliar25',['ampliar25',['../classCimahis.html#a3d63e0aba3c4b28f3e82e308aa57e5b2',1,'Cimahis']]],
  ['anchor',['anchor',['../classMorphSettingsDialog.html#a572e97b8135e97be66c31d8910b899fd',1,'MorphSettingsDialog']]],
  ['apply',['apply',['../classMorphDialog.html#aecbab45a63830c2a576d0cedcb7477d1',1,'MorphDialog::apply()'],['../classMorphOp.html#a36408e7c53c4b4a82ae7e78ff87f4c1e',1,'MorphOp::apply()']]],
  ['archivoreciente',['archivoReciente',['../classAreaImagen.html#a3c9691dfcbbd3c975987c00848662dce',1,'AreaImagen']]],
  ['areaimagen',['AreaImagen',['../classAreaImagen.html#a4f7a34a4c5ff63db03462a9eec0bcb11',1,'AreaImagen']]],
  ['asignarcamino',['asignarCamino',['../classInterfazForma.html#acc07d5ae64e475a67c240f485ca1fda5',1,'InterfazForma']]],
  ['automedianfilter',['automedianFilter',['../namespaceMorphLib.html#a9a4d0476b18248da0ae0df013f6292a8',1,'MorphLib']]],
  ['ayuda',['Ayuda',['../classAyuda.html#ae7f651d8d258d445167ba08d7c4c2ba5',1,'Ayuda::Ayuda()'],['../classCimahis.html#a41bf750378559dd6efdef45f87c91693',1,'Cimahis::ayuda()']]]
];
