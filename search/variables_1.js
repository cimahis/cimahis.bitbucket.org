var searchData=
[
  ['aceptado',['aceptado',['../classComboBox.html#ae3211ffb75a599b51fbe41e2d7e8a434',1,'ComboBox']]],
  ['action',['action',['../classInterfazForma.html#a070f0a1331a4c7de0411f2a2546e7040',1,'InterfazForma']]],
  ['actionabrir',['actionAbrir',['../classCimahis.html#ab2b134c7b242675d0d98315f3e39afc5',1,'Cimahis']]],
  ['actionacerda_5fde',['actionAcerda_de',['../classCimahis.html#ae5e8a2a07e0239288ad98a7f335ebda2',1,'Cimahis']]],
  ['actionampliar',['actionAmpliar',['../classCimahis.html#ac0f2e08cbeb4df7a7142d9ac4a64d63e',1,'Cimahis']]],
  ['actionasistido',['actionAsistido',['../classCimahis.html#a096af81f0dfcebe90579925f7dc28b85',1,'Cimahis']]],
  ['actionautomatico',['actionAutomatico',['../classCimahis.html#a9e32da60fc8cbadd0cc171f5df030685',1,'Cimahis']]],
  ['actionayuda',['actionAyuda',['../classCimahis.html#ad533235d76a41add8995aad273bbec92',1,'Cimahis']]],
  ['actioncascada',['actionCascada',['../classCimahis.html#abe8562ee65742edf8f7544234ba1f565',1,'Cimahis']]],
  ['actioncerrar',['actionCerrar',['../classCimahis.html#aa9461ac02e9d400bfa49124473568b66',1,'Cimahis']]],
  ['actioncerrartodo',['actionCerrarTodo',['../classCimahis.html#a66f918f982a03b19e503a50bddaae295',1,'Cimahis']]],
  ['actioncomadecimal',['actionComaDecimal',['../classCimahis.html#a1d69e98187b4eace007696c0ff08a358',1,'Cimahis']]],
  ['actionconfigparametrosmorfometricos',['actionConfigParametrosMorfometricos',['../classCimahis.html#ae525b24d39a6bf8bc7f2b821754c730c',1,'Cimahis']]],
  ['actionconfwave',['actionConfWave',['../classCimahis.html#af0fd47b3a85efbdd8cae13ed79427b7e',1,'Cimahis']]],
  ['actioncopiagris',['actionCopiaGris',['../classCimahis.html#a147130ebaaedfa745adb027365097000',1,'Cimahis']]],
  ['actioncopiaimagen',['actionCopiaImagen',['../classCimahis.html#a3b372e90e88f12c3191a771270930df3',1,'Cimahis']]],
  ['actioncopiar',['actionCopiar',['../classCimahis.html#a0e6eac1da584f28f252ac867b3c5c64d',1,'Cimahis']]],
  ['actiondwt',['actionDWT',['../classCimahis.html#a3cbe20d6b99fc7e8135f771ced758165',1,'Cimahis']]],
  ['actionfiltros',['actionFiltros',['../classCimahis.html#af23d4aa00ba88528ab01fedff3dfea20',1,'Cimahis']]],
  ['actiongraficacircularidad',['actionGraficaCircularidad',['../classCimahis.html#a5548ea5e9120c93d1bcbcd65598d1ad7',1,'Cimahis']]],
  ['actiongraficadiametro',['actionGraficaDiametro',['../classCimahis.html#a4d311134bfee9b111403a48dd143e71a',1,'Cimahis']]],
  ['actiongraficaejemayor',['actionGraficaEjeMayor',['../classCimahis.html#a610da47e69aea5bf85f18dbd07522081',1,'Cimahis']]],
  ['actiongraficaejemenor',['actionGraficaEjeMenor',['../classCimahis.html#a0c0785fdbb0c64b0d2782938462cc8f1',1,'Cimahis']]],
  ['actiongraficafactorforma',['actionGraficaFactorForma',['../classCimahis.html#ae8fa589995974a31c3225dd87115dc4b',1,'Cimahis']]],
  ['actiongraficaicn',['actionGraficaICN',['../classCimahis.html#a5f97457c5feb759544e8c98875c60994',1,'Cimahis']]],
  ['actiongraficamorfometria',['actionGraficaMorfometria',['../classCimahis.html#a0809c3765541668bea401acc82377afb',1,'Cimahis']]],
  ['actiongraficaperimetro',['actionGraficaPerimetro',['../classCimahis.html#a9fc3ec86032525b216579a2793dec641',1,'Cimahis']]],
  ['actiongraficarelacionab',['actionGraficaRelacionAb',['../classCimahis.html#a155bb4535c87294ca0b4c0726754cb88',1,'Cimahis']]],
  ['actiongraficatamano',['actionGraficaTamano',['../classCimahis.html#a8b77173c43f1c80a4c4bd81121006546',1,'Cimahis']]],
  ['actionguardar',['actionGuardar',['../classCimahis.html#a7b9c810895b70dfc542e71ee5197fb19',1,'Cimahis']]],
  ['actionguardar_5fcomo',['actionGuardar_Como',['../classCimahis.html#a8fa9197afabde217954bdf02122d8271',1,'Cimahis']]],
  ['actionguardarrtwavelets',['actionGuardarRTWavelets',['../classCimahis.html#a9bd01e8ac7a36f7ba6842aea4f6e706c',1,'Cimahis']]],
  ['actionhsl',['actionHSL',['../classCimahis.html#a2b37e1a5e629c12a4c15397fecc90688',1,'Cimahis']]],
  ['actionhsv',['actionHSV',['../classCimahis.html#ac328ed22fd16127ee309cc20d144ed97',1,'Cimahis']]],
  ['actionidwt',['actionIDWT',['../classCimahis.html#a9d8137ca4556d6290731971a2f167c08',1,'Cimahis']]],
  ['actionmanual',['actionManual',['../classCimahis.html#ae58076a69d30595422039164a00b7ad6',1,'Cimahis']]],
  ['actionmedirm',['actionMedirM',['../classCimahis.html#a14855f5cf3bc8b700756836693531006',1,'Cimahis']]],
  ['actionmedirp',['actionMedirP',['../classCimahis.html#a598d4c46a40faedadd79f996f4fe0cf7',1,'Cimahis']]],
  ['actionmedirparametros',['actionMedirParametros',['../classCimahis.html#a73b3e796e81113091aed27eeb9da2259',1,'Cimahis']]],
  ['actionmorf',['actionMorf',['../classCimahis.html#a6e48f34b7d306a086016855088f70a4c',1,'Cimahis']]],
  ['actionmosaico',['actionMosaico',['../classCimahis.html#aba034f91b141d4f2c91dc5813d3706d7',1,'Cimahis']]],
  ['actionnegativo',['actionNegativo',['../classCimahis.html#a2655e53f1aebf3a4cc1da14e1da3c352',1,'Cimahis']]],
  ['actionnuevaimagen',['actionNuevaImagen',['../classCimahis.html#ada9fce816f61c75393100becc8a4581c',1,'Cimahis']]],
  ['actionopenpluginmanager',['actionOpenPluginManager',['../classCimahis.html#a735a44d4feec65442508d2a06430cc5a',1,'Cimahis']]],
  ['actionpuntodecimal',['actionPuntoDecimal',['../classCimahis.html#a892fbb81cf1efea41b0cde336130e2dc',1,'Cimahis']]],
  ['actionrasgostextwavelets',['actionRasgosTextWavelets',['../classCimahis.html#a1c71c1f6a15839dc9f86c8f78bf7ee39',1,'Cimahis']]],
  ['actionreducir',['actionReducir',['../classCimahis.html#a09f7c797422b2cfd386cce36d6947ead',1,'Cimahis']]],
  ['actionreporte',['actionReporte',['../classCimahis.html#a7114794dc487f5a2d535d73d4818441d',1,'Cimahis']]],
  ['actionrgb',['actionRGB',['../classCimahis.html#a047bb03a048b00159763f7ed1b86ee2f',1,'Cimahis']]],
  ['actionsalir',['actionSalir',['../classCimahis.html#a1a0eff29b08fc6b44d888e3876bf5966',1,'Cimahis']]],
  ['actionsegmentarhistograma',['actionSegmentarHistograma',['../classCimahis.html#a1ec6fd9ae81653ad2ad9c6f5db0dc7b4',1,'Cimahis']]],
  ['actionsegmentarrgb',['actionSegmentarRGB',['../classCimahis.html#a5449788187c2b2495839ee77020345c1',1,'Cimahis']]],
  ['actionseleccionar',['actionSeleccionar',['../classCimahis.html#ae8a3965073d4ddc5cf3fd5b04f345464',1,'Cimahis']]],
  ['actionsepararcanales',['actionSepararCanales',['../classCimahis.html#a8f923f3a170ccc1697f258f0f880098a',1,'Cimahis']]],
  ['actionsuavizado',['actionSuavizado',['../classCimahis.html#a7d3be614f37289d0d660874131515239',1,'Cimahis']]],
  ['actionverhistograma',['actionVerHistograma',['../classCimahis.html#a9c8c97c557903116a3e05867ab9c6ba1',1,'Cimahis']]],
  ['alto',['alto',['../classAreaImagen.html#a4939736af8206419895f98b51c6a340d',1,'AreaImagen::alto()'],['../structnp__s.html#a96a0266afe787d8d69f93a1001944745',1,'np_s::alto()']]],
  ['ampliacion',['ampliacion',['../classAreaImagen.html#a76b720e855b88a8b19c688c08587f516',1,'AreaImagen']]],
  ['ancho',['ancho',['../classAreaImagen.html#a7b2429ef18f39aa913ef175e6fbe2c59',1,'AreaImagen::ancho()'],['../structnp__s.html#a2b1583b0c4d2d538f3dfd1361c59edf3',1,'np_s::ancho()']]],
  ['ancla',['ancla',['../structSgmUtils_1_1MorphOpParams.html#ad26523977d326447722e7f918480ef10',1,'SgmUtils::MorphOpParams']]],
  ['archreciente',['archReciente',['../classAreaImagen.html#a3cb63429a9822edb37afea72d8c90758',1,'AreaImagen']]],
  ['area',['area',['../classelegirFiltros.html#a882f2ebbfe45f1b2689ea24448a20cb7',1,'elegirFiltros::area()'],['../classVentanaUmbral.html#ae329e1138e467df9754da23cf099c67b',1,'VentanaUmbral::area()'],['../classInterfazForma.html#a949ee9a788f0693d770905c50cc397f5',1,'InterfazForma::area()'],['../structnp__s.html#a0ebe259784280be7e1fe550bb81035c9',1,'np_s::area()'],['../structparametros.html#ae2dafd81a28fa1c5dae111c1ecbf27a7',1,'parametros::area()']]],
  ['areanuevobjtsegmentd',['areaNuevObjtSegmentd',['../classAreaImagen.html#abf3c9c68401bb326b6646d98db94d159',1,'AreaImagen']]],
  ['areapf',['areaPF',['../classInterfazForma.html#a4c8783ca9ca9d620c38820068c460640',1,'InterfazForma']]],
  ['areapi',['areaPI',['../classInterfazForma.html#a02eb4088c7b635dcbfae445725d659db',1,'InterfazForma']]],
  ['areas',['areas',['../classInterfazForma.html#ad09a40afe0e444c7961faec4b3a4095d',1,'InterfazForma']]],
  ['areat',['areaT',['../classInterfazForma.html#a10060811c7b921672cc8ca9ffbbc71d7',1,'InterfazForma']]],
  ['asignadorventana',['asignadorVentana',['../classCimahis.html#a675949d6a4d2eb030d8cb79c9e7c9451',1,'Cimahis']]],
  ['auxcont',['auxCont',['../classAreaImagen.html#a0cb1d74f9317f8cab83fcb3c58436afe',1,'AreaImagen']]],
  ['auxzoom',['auxZoom',['../classAreaImagen.html#a425ca42b222078dd6969e526b877223d',1,'AreaImagen']]],
  ['ayudausuario',['ayudaUsuario',['../classCimahis.html#a66fb1daa6b31f1e1ffec03240d419fa6',1,'Cimahis']]]
];
