var searchData=
[
  ['baseimage',['baseImage',['../classSgmDialog.html#a2974557f1a6fa99734afcc3fa3ff09dd',1,'SgmDialog']]],
  ['bgr',['BGR',['../namespaceUtilsLib.html#aca5fd4abf7100d9edc65b0d8f9914b81ac0e0f4f0fed9281dc7b51406bbf05451',1,'UtilsLib']]],
  ['bordclear',['bordClear',['../namespaceMorphLib.html#aba0ca50e980f31d97b929c1c4d45ffa0',1,'MorphLib']]],
  ['borders_5fclearing',['BORDERS_CLEARING',['../namespaceMorphLib.html#acdb16937c6eec6f3f77dd09537b92f3ba8c0b604540597c6cb4d1ea866b7f9d81',1,'MorphLib']]],
  ['bottomhat',['BOTTOMHAT',['../namespaceMorphLib.html#acdb16937c6eec6f3f77dd09537b92f3baad16da9b1c6ae056834cb5660aa9569e',1,'MorphLib::BOTTOMHAT()'],['../namespaceMorphLib.html#a4f87cfb97d087d2a0808b99841f345fe',1,'MorphLib::bottomhat(const cv::Mat &amp;src, cv::Mat &amp;dst, const cv::Mat &amp;se, cv::Point anchor=cv::Point(-1,-1), int iters=1)']]],
  ['boundingrect',['boundingRect',['../classSgmGraphBox.html#a33633e8a3bf0234df85687c7ee52a644',1,'SgmGraphBox::boundingRect()'],['../classSgmGraphContour.html#a0306f71bb6855164be897def2a9dff4c',1,'SgmGraphContour::boundingRect()'],['../classSgmGraphMarker.html#a6e379c4cf8a9d3ba8c6c8014b5ef8d1d',1,'SgmGraphMarker::boundingRect()']]],
  ['box',['box',['../classSgmGraphBox.html#ab79eb6cf169bc33059e5cbeb07d6a085',1,'SgmGraphBox']]],
  ['boxbottomlefthgrip',['BoxBottomLeftHgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722aabdeeacdfe893d027c59dc70f9b3ee7a',1,'SgmGraphBox']]],
  ['boxbottommiddlehgrip',['BoxBottomMiddleHgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722a27c49f04f277cd3e99fcff115ea80c68',1,'SgmGraphBox']]],
  ['boxbottomrighthgrip',['BoxBottomRightHgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722a333baa747c7d40e3c98860ba693847d6',1,'SgmGraphBox']]],
  ['boxes',['BOXES',['../classSgmGraphPixmap.html#a5dada35c49a03c7cffd3976e798e2bc3aa16104dab42d78cc8d4fb1a4735e384c',1,'SgmGraphPixmap']]],
  ['boxhandgrip',['BoxHandgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722',1,'SgmGraphBox']]],
  ['boxleftmiddlehgrip',['BoxLeftMiddleHgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722a48b5cee9aca916527fc1f4b3513479b5',1,'SgmGraphBox']]],
  ['boxrightmiddlehgrip',['BoxRightMiddleHgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722a76275f2145b2d5929daaaa889f0c5bc1',1,'SgmGraphBox']]],
  ['boxtoplefthgrip',['BoxTopLeftHgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722a9a8df684a060d811a382ad7aabd7aafa',1,'SgmGraphBox']]],
  ['boxtopmiddlehgrip',['BoxTopMiddleHgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722a7623c51ca3aba4a613f31f6b33f0b175',1,'SgmGraphBox']]],
  ['boxtoprighthgrip',['BoxTopRightHgrip',['../classSgmGraphBox.html#a87d384f55e56ade18d96446161b28722a1490de8c5efe23d05bf16da9f1dffe23',1,'SgmGraphBox']]],
  ['buscarmenupadre',['buscarMenuPadre',['../classPluginManager.html#a12f33f35ffba9f215233d38e731b8048',1,'PluginManager']]],
  ['buscarpos',['buscarPos',['../classMorfometria.html#a680348536589924d2a37502bc1090b46',1,'Morfometria']]]
];
