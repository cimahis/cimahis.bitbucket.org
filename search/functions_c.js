var searchData=
[
  ['marker',['marker',['../classSgmGraphMarker.html#a7f966547321b92fba732e23c01a6dbac',1,'SgmGraphMarker']]],
  ['mdihijoactivo',['mdiHijoActivo',['../classCimahis.html#ae8c0cc82a0de02f3e06701e881cadaa2',1,'Cimahis']]],
  ['media',['media',['../classMorfometria.html#a082334317955579daba164cac281a236',1,'Morfometria']]],
  ['mediana',['mediana',['../classMorfometria.html#af113c4b0519819ccbd49174a062b314f',1,'Morfometria']]],
  ['medirmicras',['medirMicras',['../classCimahis.html#a4eeb2e239683e8f3a1234adc6f6e105a',1,'Cimahis']]],
  ['medirpixeles',['medirPixeles',['../classCimahis.html#a6378640a1a5b3c1aaa82aa5930627477',1,'Cimahis']]],
  ['mejoraimagen',['mejoraImagen',['../Imagen_8h.html#a48894b5dc150ec2149fc25875d98076a',1,'Imagen.h']]],
  ['menudinamico',['menuDinamico',['../classCimahis.html#aba4235e2fcd0d5acfd8dd078aa6912ce',1,'Cimahis']]],
  ['micras',['micras',['../classCimahis.html#a5c7c2b9c90b74de40370eb59ce01aa56',1,'Cimahis']]],
  ['modificararea',['modificarArea',['../classAreaImagen.html#a7f93d54eb40915b3e9046d3989902664',1,'AreaImagen']]],
  ['modificarfiltro',['modificarFiltro',['../classventanaParametrosFiltro.html#aa3f63d05da58ff7f9aa7dd33d993458c',1,'ventanaParametrosFiltro']]],
  ['modificarimagencv',['modificarImagenCv',['../classAreaImagen.html#ac8ac6c1f14ea8f1fed13f03845b89e6f',1,'AreaImagen']]],
  ['modificarimagencvaux',['modificarImagenCvAux',['../classAreaImagen.html#ab62e0f4305658f55050af22302c52f60',1,'AreaImagen']]],
  ['modificarimagenqt',['modificarImagenQt',['../classAreaImagen.html#a7e7ee8ece8c5e3549deccba0223bb404',1,'AreaImagen']]],
  ['modificarrutaarchivo',['modificarRutaArchivo',['../classAreaImagen.html#a075eb14fd75b3f019de8b38fb4de17eb',1,'AreaImagen']]],
  ['momento11',['momento11',['../classMorfometria.html#a5e6d6313823d40997ff08dc265b10f8d',1,'Morfometria']]],
  ['momento2',['momento2',['../classMorfometria.html#a4c0eee6eb7119ebefe760adb5c4921ed',1,'Morfometria']]],
  ['morfometria',['Morfometria',['../classMorfometria.html#a51c00b89c5279bb7ad0ce82ed764146b',1,'Morfometria']]],
  ['morphdialog',['MorphDialog',['../classMorphDialog.html#a27b4810eba908421feb71217e85b1114',1,'MorphDialog']]],
  ['morphgradient',['morphGradient',['../namespaceMorphLib.html#a9e6ef850b05f10f72d8af7f4390f9dd2',1,'MorphLib']]],
  ['morphop',['MorphOp',['../classMorphOp.html#ab91837569dc51c3503bb4db0fc1acc96',1,'MorphOp']]],
  ['morphopparams',['MorphOpParams',['../structSgmUtils_1_1MorphOpParams.html#aae6a5d3a4d299dc26ba4ad7a836d075e',1,'SgmUtils::MorphOpParams::MorphOpParams()'],['../structSgmUtils_1_1MorphOpParams.html#ac71654cccf7a7e7512d68849349dc440',1,'SgmUtils::MorphOpParams::MorphOpParams(int codgOp, const cv::Mat &amp;ee, cv::Point ancla, int nIters)']]],
  ['morphproc',['morphProc',['../namespaceSgmUtils.html#aef739ff44ccf270235f792eb7df8fbfd',1,'SgmUtils']]],
  ['morphprocopsdefault',['morphProcOpsDefault',['../namespaceSgmUtils.html#af9816096491c52d3aff6b55198bbbd45',1,'SgmUtils']]],
  ['morphsettingsdialog',['MorphSettingsDialog',['../classMorphSettingsDialog.html#ab9abb9291adc594097362b49c1cc0ff0',1,'MorphSettingsDialog']]],
  ['morphsmooth',['morphSmooth',['../namespaceMorphLib.html#a991155d43ef9233ed6538eeb4e1e1ca5',1,'MorphLib']]],
  ['mostrararea',['mostrarArea',['../classgraficoParametros.html#a5eef528e2ad7e31501a592b4da8a1c22',1,'graficoParametros']]],
  ['mostrarcircularidad',['mostrarCircularidad',['../classgraficoParametros.html#a2454845164d8c03bd43b597a6ee4d429',1,'graficoParametros']]],
  ['mostrardiametro',['mostrarDiametro',['../classgraficoParametros.html#ae382df75f15ead970dece7315526c3e2',1,'graficoParametros']]],
  ['mostrarejemayor',['mostrarEjeMayor',['../classgraficoParametros.html#a660fec7e1e700484370f7735ed1fc641',1,'graficoParametros']]],
  ['mostrarejemenor',['mostrarEjeMenor',['../classgraficoParametros.html#a053f36b9fb27253cdde363c65f0176a5',1,'graficoParametros']]],
  ['mostrarenmdiarea',['mostrarEnMdiArea',['../classCimahis.html#a2baad12e052de140ffcb28b8aaf0edfe',1,'Cimahis']]],
  ['mostrarfactorforma',['mostrarFactorForma',['../classgraficoParametros.html#ad95a058d731770206f991c77fbad28ba',1,'graficoParametros']]],
  ['mostraricn',['mostrarICN',['../classgraficoParametros.html#a85694aa8c56571562d521bd75b2d1fa1',1,'graficoParametros']]],
  ['mostrarimagen',['mostrarImagen',['../classAreaImagen.html#a73ef09c29808b5ba417097040df8bcfc',1,'AreaImagen::mostrarImagen(const QString &amp;nombreArchivo)'],['../classAreaImagen.html#a10f0a2c97f89982d6bbbfbba4016a54f',1,'AreaImagen::mostrarImagen(QImage *imagen, QString fileName)'],['../classCImageInterface.html#ac708234276f1d422adeeecfc12cda038',1,'CImageInterface::mostrarImagen()']]],
  ['mostrarmensaje',['mostrarMensaje',['../classCimahis.html#a53fb6b04e12c5f73b2a771f4e78e7c12',1,'Cimahis::mostrarMensaje()'],['../classInterfazForma.html#aaf46020f70eb30e0e89b436aa50fa629',1,'InterfazForma::mostrarMensaje()']]],
  ['mostrarperimetro',['mostrarPerimetro',['../classgraficoParametros.html#aed5cd7e16525afb269871e5409a7900d',1,'graficoParametros']]],
  ['mostrarrelacionab',['mostrarRelacionAb',['../classgraficoParametros.html#ab87064117fdc5607126b8b0651f54815',1,'graficoParametros']]],
  ['mostrartamano',['mostrarTamano',['../classgraficoParametros.html#aacbb58ae1d35e9a146721a0e21549d6f',1,'graficoParametros']]],
  ['mouseaddibleitems',['mouseAddibleItems',['../classSgmGraphPixmap.html#a6cc3bea51eff5bc7d84248fbd8aa77c5',1,'SgmGraphPixmap']]],
  ['mouseaddsitemsenabled',['mouseAddsItemsEnabled',['../classSgmGraphPixmap.html#a28cf97ce7703300885c7aa2b70c54572',1,'SgmGraphPixmap']]],
  ['mousemoved',['mouseMoved',['../classInterfazForma.html#a02c20961b7d6491f9f79f89c0393cd1f',1,'InterfazForma']]],
  ['mousemoveevent',['mouseMoveEvent',['../classAreaImagen.html#a03d07f5fcd5c70b22a88ca2b26b07c57',1,'AreaImagen::mouseMoveEvent()'],['../classQGraphicsViewPersonalizado.html#acc996db07c2a2e78074e21263043f074',1,'QGraphicsViewPersonalizado::mouseMoveEvent()'],['../classSgmGraphBox.html#ab4706aea4e180494cb55903cd7e98f48',1,'SgmGraphBox::mouseMoveEvent()'],['../classSgmGraphPixmap.html#af1506d9c9abc162f0656be100fed22f7',1,'SgmGraphPixmap::mouseMoveEvent()']]],
  ['mousemovement',['mouseMovement',['../classQGraphicsViewPersonalizado.html#a5423de3b950a57f32b752d855e71dfdb',1,'QGraphicsViewPersonalizado']]],
  ['mousepressed',['mousePressed',['../classInterfazForma.html#a1757b6ac442bbbaca79012d1d911e83d',1,'InterfazForma']]],
  ['mousepressevent',['mousePressEvent',['../classAreaImagen.html#abfe467f1ce0f839a39206b55ffdd2256',1,'AreaImagen::mousePressEvent()'],['../classQGraphicsViewPersonalizado.html#a0ec23979124c45ad59855fef8bc940c5',1,'QGraphicsViewPersonalizado::mousePressEvent()'],['../classSgmGraphBox.html#a369ff5e8f218fcdbb963f78098a9d8a2',1,'SgmGraphBox::mousePressEvent()'],['../classSgmGraphPixmap.html#a406eae477ef36e40b325809d0dda1659',1,'SgmGraphPixmap::mousePressEvent()']]],
  ['mousepressing',['mousePressing',['../classQGraphicsViewPersonalizado.html#a28758d68aca7f0f4a1b6b954ea2cdb7c',1,'QGraphicsViewPersonalizado']]],
  ['mousereleased',['mouseReleased',['../classInterfazForma.html#aad55a646ba2ee8358ddc52a38009c151',1,'InterfazForma']]],
  ['mousereleaseevent',['mouseReleaseEvent',['../classAreaImagen.html#a95e867569b413c170d69ae6a083dae33',1,'AreaImagen::mouseReleaseEvent()'],['../classQGraphicsViewPersonalizado.html#a09ecb2a80d83ddb9db506d3666f7590e',1,'QGraphicsViewPersonalizado::mouseReleaseEvent()'],['../classSgmGraphBox.html#aaf3e74af1c076293149936b8742c6eac',1,'SgmGraphBox::mouseReleaseEvent()'],['../classSgmGraphPixmap.html#ab9baab5b536c75f29d7a26a5a4f7eff5',1,'SgmGraphPixmap::mouseReleaseEvent()']]],
  ['mousereleasing',['mouseReleasing',['../classQGraphicsViewPersonalizado.html#a574459045f2fc2a7c4ef5972a80b8b08',1,'QGraphicsViewPersonalizado']]]
];
