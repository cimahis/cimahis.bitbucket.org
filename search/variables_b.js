var searchData=
[
  ['name',['NAME',['../namespacePluginProperty.html#a60f75e8da9a1f2d7fa4f44adf4625df0',1,'PluginProperty']]],
  ['nbarras',['nBarras',['../classhistograma.html#a4e26ac5d2219855093d61e2abafcad78',1,'histograma']]],
  ['nextact',['nextAct',['../classCimahis.html#a9ef9e0885e95b96ea493bee20bfc89b3',1,'Cimahis']]],
  ['niters',['nIters',['../structSgmUtils_1_1MorphOpParams.html#ae995961a8f4c5aa42c022e63b55b08f6',1,'SgmUtils::MorphOpParams']]],
  ['niveles',['niveles',['../classAreaImagen.html#a512b2791013f1279ae1395da674aa54a',1,'AreaImagen']]],
  ['nombre',['nombre',['../classfiltros.html#a9390cb96f308d1103b8361cf1ced9334',1,'filtros']]],
  ['nombreimagen',['nombreImagen',['../classInterfazForma.html#ae7fa0de73e5a3bddce2caf0997adfbad',1,'InterfazForma']]],
  ['nombreplantilla',['nombrePlantilla',['../classConfigurarWavelets.html#a3d855275d95d34c7ef78a5459eeb9ce8',1,'ConfigurarWavelets']]],
  ['nombreplantillawavelet',['nombrePlantillaWavelet',['../classAreaImagen.html#a5c8ee55a2e0957418b1127f7440b04e5',1,'AreaImagen']]],
  ['nparametrosextra',['nParametrosExtra',['../classfiltros.html#afd89223b074b7e2b421fbad193c005a7',1,'filtros']]],
  ['numfiltros',['numFiltros',['../classConfigurarWavelets.html#a77087ffd6f07b116284dffcc5578cadd',1,'ConfigurarWavelets']]],
  ['numniveles',['numNiveles',['../classConfigurarWavelets.html#a0ec228deddd519188b29e862bc42bbc4',1,'ConfigurarWavelets']]],
  ['numobjssegmt',['numObjsSegmt',['../classThreadSgm.html#a6a0b7f78875731a3cfe2086fcb43967d',1,'ThreadSgm']]],
  ['numregnsinters',['numRegnsInters',['../classAreaImagen.html#a3f0f3820cad211b4fd563408c39f7c75',1,'AreaImagen']]],
  ['nvalores',['nValores',['../classhistograma.html#a345f9282f0cc75236023679952798d14',1,'histograma']]]
];
