var searchData=
[
  ['ra',['RA',['../structnp__s.html#ae38d6c5174039e757dc100896ecd8624',1,'np_s::RA()'],['../structparametros.html#a80d5cac5611b2ab7b8a016fe50a3bd2c',1,'parametros::RA()']]],
  ['rasgostextura',['rasgosTextura',['../classAreaImagen.html#a05c75b1c2c2ab58e4df245c032aac4a9',1,'AreaImagen::rasgosTextura()'],['../classConfigurarWavelets.html#af5439c5a1d2976ec27e23d4544fa7784',1,'ConfigurarWavelets::rasgosTextura()']]],
  ['recta',['recta',['../classAreaImagen.html#a645eb67192bde4ec35d87286820d27ee',1,'AreaImagen']]],
  ['rectcontndr',['rectContndr',['../classAreaImagen.html#ab25859e554548828ac03d8b1d3c35eb8',1,'AreaImagen']]],
  ['regnsinters',['regnsInters',['../classAreaImagen.html#a0d6da02c737b0c129711081b8afe2097',1,'AreaImagen']]],
  ['rel_5fdate',['REL_DATE',['../namespacePluginProperty.html#ac4691b38c5d0bb4833fa25ffb7c652bf',1,'PluginProperty']]],
  ['reporte',['reporte',['../classAreaImagen.html#a0f9d074e66112fdb132cd710ff5efe40',1,'AreaImagen']]],
  ['row',['row',['../structNodo.html#a93695045db26664b21f21ba768c13a5f',1,'Nodo']]],
  ['rutaimagen',['rutaImagen',['../classCimahis.html#ab000d0caf8a2361367a0ffde79899097',1,'Cimahis']]]
];
