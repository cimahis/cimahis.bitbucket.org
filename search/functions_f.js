var searchData=
[
  ['paint',['paint',['../classSgmGraphBox.html#a39662085f4a69b7bddb8d8eee4777b16',1,'SgmGraphBox::paint()'],['../classSgmGraphContour.html#ac2fc4d8b7d222992a599301757147718',1,'SgmGraphContour::paint()'],['../classSgmGraphMarker.html#aebea65fdc3982a2b9354933ccd163a6f',1,'SgmGraphMarker::paint()']]],
  ['paintevent',['paintEvent',['../classAreaImagen.html#a7b2c11c37d4686a1cfe2996a52184284',1,'AreaImagen::paintEvent()'],['../classelegirFiltros.html#ab625665e0e2ffc23b8d7898055120b8d',1,'elegirFiltros::paintEvent()'],['../classlienzoHistograma.html#a0e89dbb40903bb0615e4b19157428c8f',1,'lienzoHistograma::paintEvent()']]],
  ['pasarapuntador',['pasarApuntador',['../classhistograma.html#a2fa2ebd61ef41b36e05fcabf9296e5b0',1,'histograma']]],
  ['pasardatos',['pasarDatos',['../classthreadFiltro.html#a446ddc3d39d14bbbc479a25158ea7fd3',1,'threadFiltro']]],
  ['penwidth',['penWidth',['../classSgmGraphItem.html#a4623f97d769f56c5427ebc9b12014149',1,'SgmGraphItem']]],
  ['pintarcamino',['pintarCamino',['../classInterfazForma.html#ade569d0af7f32757719fe821c90fa579',1,'InterfazForma::pintarCamino(QVector&lt; QPoint &gt; *c)'],['../classInterfazForma.html#a551cb02795e585ff4fa41a3aa859b4a6',1,'InterfazForma::pintarCamino(vector&lt; Point &gt; *c)']]],
  ['pintarcaminodesdearchivo',['pintarCaminoDesdeArchivo',['../classInterfazForma.html#ad5abf31d661991af8880310766c01971',1,'InterfazForma']]],
  ['pixlptr',['pixlPtr',['../namespaceSgmUtils.html#aa540c0d93421d33126f20f48cbc565b9',1,'SgmUtils']]],
  ['pixlval',['pixlVal',['../namespaceSgmUtils.html#a14adfb0cdfb4af447129375a6e0d3492',1,'SgmUtils']]],
  ['pluginmanager',['PluginManager',['../classPluginManager.html#a28d1e72c90d61400df26eaba6b7de683',1,'PluginManager::PluginManager(QWidget *parent=0)'],['../classPluginManager.html#a6627683ad22be40425124c0be87a9ed9',1,'PluginManager::PluginManager(QWidget *parent, PluginMap *mapaPlugins)'],['../classPluginManager.html#ae77cfc658dc9c376e163e1fa3520bdf5',1,'PluginManager::PluginManager(QWidget *parent, PluginHandler *ph, PluginMap *pm, PointerMap *ptrm)']]],
  ['pnpoly',['pnpoly',['../classInterfazForma.html#a80e5c0dd2b94dde9d4dcea4e452119d5',1,'InterfazForma']]],
  ['posibleguardar',['posibleGuardar',['../classAreaImagen.html#a6a18ac2d391f84148516777abfdda526',1,'AreaImagen']]],
  ['prepararinterfaz',['prepararInterfaz',['../classVisualizarHistograma.html#a6cde2139e001712327678aaede2f2b59',1,'VisualizarHistograma']]],
  ['presetdeconvl',['presetDeconvl',['../namespaceSgmUtils.html#aec23ef28ebe9ae83953160616b1986e3',1,'SgmUtils']]],
  ['presetthick',['presetThick',['../namespaceMorphLib.html#afe825ed33e492ab106cade932f6649f1',1,'MorphLib']]],
  ['promedio',['promedio',['../classMorfometria.html#a2638a2b05c7c90fb341e4eee7a54bd65',1,'Morfometria']]],
  ['properclose',['properClose',['../namespaceMorphLib.html#ade991878707652406f871fa0d8e53259',1,'MorphLib']]],
  ['properopen',['properOpen',['../namespaceMorphLib.html#a7f94c9d4fa36bfb5daa5ac310989069e',1,'MorphLib']]],
  ['punto',['punto',['../classpunto.html#a6730ee3ec15e233dc894cbe2e780786f',1,'punto::punto(float x, float y)'],['../classpunto.html#aaef8db478cacdb429ca5fe4f3674bc22',1,'punto::punto()']]],
  ['puntoprocesado',['puntoProcesado',['../classInterfazForma.html#a78adf95e910127d09a05d51f0dd6f30f',1,'InterfazForma']]]
];
