var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "acefghilmnpqrstv",
  2: "cmpsu",
  3: "acefghilmpqrstuvw",
  4: "abcdefghijklmnopqrstuvwz~",
  5: "_acdefghilmnoprstuvwxyz",
  6: "cdmnp",
  7: "abcos",
  8: "abcdeghmoprst",
  9: "adeilmnpt",
  10: "chuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Estructuras de Datos",
  2: "Namespaces",
  3: "Archivos",
  4: "Funciones",
  5: "Variables",
  6: "&apos;typedefs&apos;",
  7: "Enumeraciones",
  8: "Valores de enumeraciones",
  9: "&apos;defines&apos;",
  10: "Páginas"
};

