var searchData=
[
  ['default_5fzoom',['DEFAULT_ZOOM',['../classSgmView.html#a661b327b70131d49ffa26070148b1ba0',1,'SgmView']]],
  ['desc',['DESC',['../namespacePluginProperty.html#a56c4e7271b87bd90d86642c37ed4b708',1,'PluginProperty']]],
  ['dev_5femail',['DEV_EMAIL',['../namespacePluginProperty.html#a5f304a0becdcda6c747bd13378fbc13a',1,'PluginProperty']]],
  ['dev_5fname',['DEV_NAME',['../namespacePluginProperty.html#ac6ab7aa46f780d5d3e3537b6b9efc239',1,'PluginProperty']]],
  ['devs',['DEVS',['../namespacePluginProperty.html#a29292162188b28a541b4e93867c2df8e',1,'PluginProperty']]],
  ['diametro',['diametro',['../structnp__s.html#aafaf2a9dff7024aa066954389708b3c4',1,'np_s::diametro()'],['../structparametros.html#a999ecee745c46a3ad0e77b5565e01953',1,'parametros::diametro()']]],
  ['dimensiones',['dimensiones',['../classfiltros.html#ae99125719bbb221444121be5102acc8d',1,'filtros']]],
  ['dockwidgetarea',['dockWidgetArea',['../classCimahis.html#a9a6ba1fdb69e7b7a1c4fc8e685151fea',1,'Cimahis']]],
  ['dockwidgetarea2',['dockWidgetArea2',['../classCimahis.html#a9ca44c8919f8f9e5c6793399c5fdf5d3',1,'Cimahis']]]
];
