var searchData=
[
  ['hgripassoccursshap',['hgripAssocCursShap',['../classSgmGraphBox.html#a10524edf66f60dd096f286b5e7736342',1,'SgmGraphBox']]],
  ['highlightcolor',['highLightColor',['../classSgmGraphItem.html#a37e86ab7cacbae08ff92ba179052224a',1,'SgmGraphItem']]],
  ['histogramaestandar',['histogramaEstandar',['../classhistogramaEstandar.html#a334cf0fb181fd390cacd986e3045b4e1',1,'histogramaEstandar']]],
  ['histogramatinte',['histogramaTinte',['../classhistogramaTinte.html#aba33f135f08fcdc7fd99eb61155afc71',1,'histogramaTinte']]],
  ['hitmiss',['hitMiss',['../namespaceMorphLib.html#a5e8b31ec8113243cded11a20f4054ffe',1,'MorphLib']]],
  ['holefill',['holeFill',['../namespaceMorphLib.html#a0e2874fbdbe4179671a2e08149089874',1,'MorphLib']]],
  ['hoverenterevent',['hoverEnterEvent',['../classSgmGraphItem.html#aa4b427c7048ac41d878ca24cd22ba09a',1,'SgmGraphItem']]],
  ['hoverleaveevent',['hoverLeaveEvent',['../classSgmGraphItem.html#ac4b7ff420bd4719ecc8b096fc9df6e71',1,'SgmGraphItem']]],
  ['hovermoveevent',['hoverMoveEvent',['../classSgmGraphBox.html#a6559f89fefde6a1d71e89b485d2bf497',1,'SgmGraphBox']]],
  ['hsl',['HSL',['../classCimahis.html#a236a18804cd77c32385c310f9c2d878a',1,'Cimahis']]],
  ['hsv',['HSV',['../classCimahis.html#aef2a07eefa1f4288f827130bc4256a24',1,'Cimahis']]]
];
