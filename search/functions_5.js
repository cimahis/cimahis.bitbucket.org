var searchData=
[
  ['fabricaobjetos',['fabricaObjetos',['../classfabricaObjetos.html#a26670a33beae05a507d1d7bdbe21a086',1,'fabricaObjetos']]],
  ['factorforma',['factorForma',['../classMorfometria.html#aaefddd41138f973be061891f78dda4ad',1,'Morfometria']]],
  ['fijarparametroextra',['fijarParametroExtra',['../classfiltros.html#ab07ac118c67a31e93630ff3eb31879af',1,'filtros']]],
  ['fijartamanokernel',['fijarTamanoKernel',['../classfiltros.html#a152bf2ddc08e4a7424d0659856bbec81',1,'filtros']]],
  ['fijarumbraloptimo',['fijarUmbralOptimo',['../classVentanaUmbral.html#a0da07b34a5dbca8adb458b1867202817',1,'VentanaUmbral']]],
  ['fillopscombobox',['fillOpsComboBox',['../classMorphDialog.html#a0b23cd72cb210edd5bd6a75584b0c6b9',1,'MorphDialog']]],
  ['fillshapecombobox',['fillShapeComboBox',['../classMorphSettingsDialog.html#a1283b04c12c3efbd141f73dd393a88ae',1,'MorphSettingsDialog']]],
  ['filtrar',['filtrar',['../classfiltros.html#ad65705200cba48f88a20d875f7f72521',1,'filtros::filtrar()'],['../classfiltroMediana.html#a86b4ee9223272b7eafb13da65f766e99',1,'filtroMediana::filtrar()'],['../classfiltroGauss.html#a45f0714152fdc7d3309a5730891c2c51',1,'filtroGauss::filtrar()'],['../classfiltroBilateral.html#a835619b0b58d3a5390e914f8c2089b21',1,'filtroBilateral::filtrar()'],['../classfiltroMeanShift.html#a06d0e41c57338c5587adca0dd860ff83',1,'filtroMeanShift::filtrar()']]],
  ['filtrobilateral',['filtroBilateral',['../classfiltroBilateral.html#ae057bdd7e364b686dd55166a3548a03b',1,'filtroBilateral']]],
  ['filtrogauss',['filtroGauss',['../classfiltroGauss.html#afc869f59eed67689b8388c040842c8bc',1,'filtroGauss']]],
  ['filtromeanshift',['filtroMeanShift',['../classfiltroMeanShift.html#af010fab356a3af55dd8600634ef5ea6a',1,'filtroMeanShift']]],
  ['filtromediana',['filtroMediana',['../classfiltroMediana.html#a3ac20bd1470b264a19f21a8aea8b7092',1,'filtroMediana']]],
  ['filtros',['filtros',['../classfiltros.html#aebfcfa19d206b414c588aac4fdbc6491',1,'filtros::filtros()'],['../classfiltros.html#a6a60e805ba660635694d59e1461e66d7',1,'filtros::filtros(std::string nombreFiltro)']]],
  ['finished',['finished',['../classMorphOp.html#a8b89cf70dfb538f66451e405f74be065',1,'MorphOp']]]
];
