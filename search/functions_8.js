var searchData=
[
  ['idvecino',['idVecino',['../classInterfazForma.html#aec3fce51f8d3ad37d869ae08c8cc991a',1,'InterfazForma']]],
  ['idwt',['idwt',['../classCimahis.html#a82dec38c27093f8a504eb1b50d91bad9',1,'Cimahis']]],
  ['idwtconplantilla',['idwtConPlantilla',['../Wavelet_8h.html#a9e87dbdbbac05614b65d381890007820',1,'Wavelet.h']]],
  ['indicecontornonuclear',['indiceContornoNuclear',['../classMorfometria.html#a412132f4f65b0e33b16ba5e663a5a4d0',1,'Morfometria']]],
  ['instalarplugin',['instalarPlugin',['../classPluginManager.html#aebd61847ef27f8fac13a15bacd56f16d',1,'PluginManager']]],
  ['intercambiar',['intercambiar',['../classMorfometria.html#add2d16a0b67e4468ae4bacd19dbe5780',1,'Morfometria']]],
  ['interfazforma',['InterfazForma',['../classInterfazForma.html#a02f9b710a8c5a756cad2b269b42b72cf',1,'InterfazForma']]],
  ['iplimage2qimage',['IplImage2QImage',['../interfazSistema_8h.html#a14432c4d7b162d6e665109a1b88d7b02',1,'interfazSistema.h']]],
  ['iplimagetoqimage',['IplImageToQImage',['../interfazSistema_8h.html#a6e29c56e9bdcd4db41b6b2bc8ddd1731',1,'interfazSistema.h']]],
  ['ischekedarea',['isChekedArea',['../classVentanaConfigParametros.html#a6c0eaa32ea83ff48c990381f4a3c3646',1,'VentanaConfigParametros']]],
  ['ischekedcircularidad',['isChekedCircularidad',['../classVentanaConfigParametros.html#aaa7d8a7b2b17d1a12b4095511f021811',1,'VentanaConfigParametros']]],
  ['ischekeddiametro',['isChekedDiametro',['../classVentanaConfigParametros.html#a05db25228bcbc2ad2891d9ddc0b383c8',1,'VentanaConfigParametros']]],
  ['ischekedejes',['isChekedEjes',['../classVentanaConfigParametros.html#ab996b7c3676985009b5aa8eb0a1600a2',1,'VentanaConfigParametros']]],
  ['ischekedfactorforma',['isChekedFactorForma',['../classVentanaConfigParametros.html#aa1179c12a7933997bad4cbacee2522de',1,'VentanaConfigParametros']]],
  ['ischekedindicecontorno',['isChekedIndiceContorno',['../classVentanaConfigParametros.html#af6fde20b0bc989f01f99bb59374bffa7',1,'VentanaConfigParametros']]],
  ['ischekedmicras',['isChekedMicras',['../classVentanaConfigParametros.html#adac7bcaef0ddae6ee98396f2ef359c4f',1,'VentanaConfigParametros']]],
  ['ischekedperimetro',['isChekedPerimetro',['../classVentanaConfigParametros.html#a66847e4556563b34714a2a5e12e3bc47',1,'VentanaConfigParametros']]],
  ['ischekedra',['isChekedRA',['../classVentanaConfigParametros.html#a12b6852a22d519697c5bd77ade1f72d9',1,'VentanaConfigParametros']]],
  ['ischekedtamanio',['isChekedTamanio',['../classVentanaConfigParametros.html#a946c1637be56c1745adbafb7d6bb9e93',1,'VentanaConfigParametros']]],
  ['ispresetop',['isPresetOp',['../namespaceMorphLib.html#ae73329a6601fc8e4537e655c033ea725',1,'MorphLib']]],
  ['iterations',['iterations',['../classMorphSettingsDialog.html#a50ac3d27c8f5f46ba14e49904c2ffc2e',1,'MorphSettingsDialog']]]
];
