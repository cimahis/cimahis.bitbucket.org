var searchData=
[
  ['derivadas',['Derivadas',['../classCostos.html#ab51b5e305c265369c7b1bb68866af533',1,'Costos']]],
  ['desest',['desEst',['../classMorfometria.html#afe41b80c8eb019cc13a4ec654d0d2124',1,'Morfometria']]],
  ['desinstalarplugin',['desinstalarPlugin',['../classPluginManager.html#a62e2fc1c502148d865d8e4b053b68584',1,'PluginManager']]],
  ['dialogomorfologia',['dialogoMorfologia',['../classCimahis.html#a2ba2616eea510b8e3d3314e5bdda28a0',1,'Cimahis']]],
  ['diametro',['diametro',['../classMorfometria.html#a5acb3bf06cdf5cb3ef3e39395b14da97',1,'Morfometria']]],
  ['dibujargrupo',['dibujarGrupo',['../classhistograma.html#a27a178e33c989f35c5ce7c51afc0fdb1',1,'histograma']]],
  ['dibujarhist',['dibujarHist',['../classhistograma.html#a263f12af46d78b1dc6c054c1086191c9',1,'histograma::dibujarHist()'],['../classhistogramaTinte.html#aa51e4df0820d59252a03e875a3d18925',1,'histogramaTinte::dibujarHist()'],['../classhistogramaEstandar.html#a962126276c6005ccd06e7e65a917d750',1,'histogramaEstandar::dibujarHist()']]],
  ['dilate',['dilate',['../namespaceMorphLib.html#a7bbc08e121c2f5d664fb5be15bf5dfe0',1,'MorphLib']]],
  ['direcciongradiente',['direccionGradiente',['../classCostos.html#a28523bff29ae72cab84e4d8b1c29ef28',1,'Costos']]],
  ['distance',['distance',['../classSgmGraphBox.html#a392d7c7bd32ac671d649e6d195ee4de6',1,'SgmGraphBox']]],
  ['dividirusandoregex',['dividirUsandoRegex',['../classCimahis.html#ad33e6c79aee3d0db6df5dba02785edd8',1,'Cimahis::dividirUsandoRegex()'],['../classPluginHandler.html#a284bf1b047cbf6ae7de940628016a0c6',1,'PluginHandler::dividirUsandoRegex()']]],
  ['dwt',['dwt',['../classCimahis.html#a795b0d2560a3c311971570da4ebf3a39',1,'Cimahis']]],
  ['dwtconplantilla',['dwtConPlantilla',['../Wavelet_8h.html#af4bc5964dcbe3e4488a360619336ee71',1,'Wavelet.h']]]
];
