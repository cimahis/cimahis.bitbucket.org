var searchData=
[
  ['label',['label',['../classSgmGraphItem.html#abf406388bf5acac3a97ac8739111924d',1,'SgmGraphItem']]],
  ['leerarchivo',['leerArchivo',['../classMorfometria.html#a7e36cdf822f7d2b33608b03569ad918d',1,'Morfometria']]],
  ['leercodcadenas',['leerCodCadenas',['../classMorfometria.html#a75be528b3c95400675d664d2422db254',1,'Morfometria']]],
  ['leerfiltro',['leerFiltro',['../classventanaParametrosFiltro.html#ad3bb685a16a88c1b5f8851def9c217ae',1,'ventanaParametrosFiltro']]],
  ['leerplugin',['leerPlugin',['../classCimahis.html#a55b9b998d14b20914dd5445ab34cd94a',1,'Cimahis::leerPlugin()'],['../classPluginHandler.html#a07f6b33600c5cdc01387b2a5fe4e7ffe',1,'PluginHandler::leerPlugin()'],['../classPluginManager.html#a2477884ff52c6dd1e9fc161530cbed93',1,'PluginManager::leerPlugin()']]],
  ['liberarpuntos',['liberarPuntos',['../classInterfazForma.html#a801182dffed9b2d5c99eb3e6725de566',1,'InterfazForma']]],
  ['liberartijeras',['liberarTijeras',['../classInterfazForma.html#af2cdc13cac70f1608ecace413c1024a4',1,'InterfazForma']]],
  ['lienzohistograma',['lienzoHistograma',['../classlienzoHistograma.html#a1fba7d9fea597fabdcb2d137289334ea',1,'lienzoHistograma']]],
  ['limpiar',['Limpiar',['../classCostos.html#a06f88b2299b3deaa49f99129f93782f8',1,'Costos']]],
  ['livewire',['Livewire',['../classLivewire.html#a7e3725ee88d5050d62f8584401235d2b',1,'Livewire']]],
  ['llamarmetodo',['llamarMetodo',['../classCimahis.html#a97a3a582a5316662ec63cf181abe47ee',1,'Cimahis::llamarMetodo()'],['../classPluginHandler.html#ab328153df607e417b0f08510309e0c46',1,'PluginHandler::llamarMetodo()']]],
  ['llamarparametros',['llamarParametros',['../classCimahis.html#a0035963ac9496af2699bb444ba5c567c',1,'Cimahis']]],
  ['localmaxs',['localMaxs',['../namespaceSgmUtils.html#a580a160f43328aa790f2f3d08c8b5d1d',1,'SgmUtils']]],
  ['localmins',['localMins',['../namespaceSgmUtils.html#ad81f39542dbe35ea37110f20a85ca70d',1,'SgmUtils']]],
  ['log',['log',['../classCLogInterface.html#a56d9d2fde87af49f09e0c7660b4da60a',1,'CLogInterface']]]
];
