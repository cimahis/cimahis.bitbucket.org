var searchData=
[
  ['qgraphicsviewpersonalizado',['QGraphicsViewPersonalizado',['../classQGraphicsViewPersonalizado.html',1,'QGraphicsViewPersonalizado'],['../classQGraphicsViewPersonalizado.html#ac2dc3a2fae1ab204431a1602322a94d3',1,'QGraphicsViewPersonalizado::QGraphicsViewPersonalizado()']]],
  ['qgraphicsviewpersonalizado_2eh',['qgraphicsviewpersonalizado.h',['../qgraphicsviewpersonalizado_8h.html',1,'']]],
  ['qimage2iplimage',['qImage2IplImage',['../interfazSistema_8h.html#adacf6c722f1646db43b22df0599e517e',1,'interfazSistema.h']]],
  ['qimage_5fiplimage',['qImage_IplImage',['../interfazSistema_8h.html#a3268d60e4442fc3eb4a9bcea525345bc',1,'interfazSistema.h']]],
  ['qimagetoiplimage',['qImageToIplImage',['../interfazSistema_8h.html#a067a8c4d686eeeb8fbbae150a715bf12',1,'interfazSistema.h']]],
  ['qimgtocvimg',['qImgtoCvImg',['../namespaceUtilsLib.html#a7734c6c1aeaf2c41997cb5303ee5601e',1,'UtilsLib']]],
  ['qlist',['QList',['../classQList.html',1,'']]],
  ['qlist_3c_20morphopparams_20_3e',['QList&lt; MorphOpParams &gt;',['../classQList.html',1,'']]],
  ['qlist_3c_20qgraphicsellipseitem_20_2a_20_3e',['QList&lt; QGraphicsEllipseItem * &gt;',['../classQList.html',1,'']]],
  ['qlist_3c_20qgraphicsrectitem_20_2a_20_3e',['QList&lt; QGraphicsRectItem * &gt;',['../classQList.html',1,'']]],
  ['qlist_3c_20qgraphicstextitem_20_2a_20_3e',['QList&lt; QGraphicsTextItem * &gt;',['../classQList.html',1,'']]],
  ['qlist_3c_20sgmgraphbox_20_2a_20_3e',['QList&lt; SgmGraphBox * &gt;',['../classQList.html',1,'']]],
  ['qlist_3c_20sgmgraphcontour_20_2a_20_3e',['QList&lt; SgmGraphContour * &gt;',['../classQList.html',1,'']]],
  ['qlist_3c_20sgmgraphmarker_20_2a_20_3e',['QList&lt; SgmGraphMarker * &gt;',['../classQList.html',1,'']]]
];
