var searchData=
[
  ['i',['i',['../classthreadFiltro.html#a617b9aa28bade100627f31f01618a19d',1,'threadFiltro::i()'],['../classInterfazForma.html#a8a5b1c7c8d2d673e3be91caca42b9ec4',1,'InterfazForma::i()']]],
  ['iconodeshabilitado',['iconoDeshabilitado',['../classPluginManager.html#a44cbc326ecc47c0d2ae818e6024f906c',1,'PluginManager']]],
  ['iconodesinstalado',['iconoDesinstalado',['../classPluginManager.html#a55be6567380218c537e88ec9f2025848',1,'PluginManager']]],
  ['iconoinstalado',['iconoInstalado',['../classPluginManager.html#adeaef6eafd696492030bc3e018b16912',1,'PluginManager']]],
  ['idvecino',['idVecino',['../classInterfazForma.html#aec3fce51f8d3ad37d869ae08c8cc991a',1,'InterfazForma']]],
  ['idwt',['idwt',['../classCimahis.html#a82dec38c27093f8a504eb1b50d91bad9',1,'Cimahis']]],
  ['idwtconplantilla',['idwtConPlantilla',['../Wavelet_8h.html#a9e87dbdbbac05614b65d381890007820',1,'Wavelet.h']]],
  ['imagen_2eh',['Imagen.h',['../Imagen_8h.html',1,'']]],
  ['imagencanal1',['imagenCanal1',['../classVisualizarHistograma.html#ae567f7763e1cee51c4150323ccf60f50',1,'VisualizarHistograma']]],
  ['imagencanal2',['imagenCanal2',['../classVisualizarHistograma.html#a066fdea7589fbc16c83a9d8d0d097a85',1,'VisualizarHistograma']]],
  ['imagencanal3',['imagenCanal3',['../classVisualizarHistograma.html#a8f7b0cc42ea12694eb1bc80b78c7c445',1,'VisualizarHistograma']]],
  ['imagenfiltrada',['imagenFiltrada',['../classelegirFiltros.html#a97c631ad2310bb17a77bf9c45507c33e',1,'elegirFiltros']]],
  ['imageninterfaz',['imagenInterfaz',['../classelegirFiltros.html#a49a7ffa72f15ff360d027f4ddd6d03b6',1,'elegirFiltros::imagenInterfaz()'],['../classVentanaUmbral.html#a550a3683590da8420585708d830f83d6',1,'VentanaUmbral::imagenInterfaz()'],['../classVisualizarHistograma.html#a2dcf578a3157615aae131ab7884d4cd6',1,'VisualizarHistograma::imagenInterfaz()']]],
  ['imagenmezcla',['imagenMezcla',['../classVisualizarHistograma.html#a87b942d0be4738f1dd4748c780850367',1,'VisualizarHistograma']]],
  ['imagenoriginal',['imagenOriginal',['../classVentanaUmbral.html#a66f69fd33a71f8b0bb422633f59241f9',1,'VentanaUmbral']]],
  ['imagenoriginalreducida',['imagenOriginalReducida',['../classVentanaUmbral.html#ac5663f1ada073d45d6002dc1333c9e48',1,'VentanaUmbral']]],
  ['imagensegmentada',['imagenSegmentada',['../classVentanaUmbral.html#a546e4aff14e9785551a11de8c637ac7e',1,'VentanaUmbral']]],
  ['imgcuadrada',['imgCuadrada',['../classConfigurarWavelets.html#ac7c2c20b06f1c3345e86fecec7d6f89d',1,'ConfigurarWavelets']]],
  ['imgcv',['imgCv',['../classAreaImagen.html#a9a9c1fa2910a885a5eb20b1d28a80db9',1,'AreaImagen']]],
  ['imgcvaux',['imgCvAux',['../classAreaImagen.html#a001dc15877200be7eab1b59cc04396fd',1,'AreaImagen']]],
  ['imgoriginal',['imgOriginal',['../classInterfazForma.html#af480019e72abe5b4278f498518a6bacb',1,'InterfazForma']]],
  ['imgqt',['imgQt',['../classAreaImagen.html#ad32e03f5e075592ab4e829e77bd7f743',1,'AreaImagen']]],
  ['imgsegmentada',['imgSegmentada',['../classAreaImagen.html#af5862344002715b235d56b944b5d23b5',1,'AreaImagen']]],
  ['imgsegmt',['imgSegmt',['../classThreadSgm.html#a663cca1c7df1dbd48c487fbfb709d366',1,'ThreadSgm']]],
  ['imgsoportsegmentacion',['imgSoportSegmentacion',['../classAreaImagen.html#a12c6907026ce82d856abfb5e095e40f5',1,'AreaImagen']]],
  ['imgsrc',['imgSrc',['../classThreadSgm.html#a9c80b383c3a00880cdc6e8ea7a3941bf',1,'ThreadSgm']]],
  ['indicecn',['indiceCN',['../structnp__s.html#a55efe4242ec94c1a90507a1e73410d09',1,'np_s::indiceCN()'],['../structparametros.html#a96d9e553676696b2b9d7893472afe6b6',1,'parametros::indiceCN()']]],
  ['indicecontornonuclear',['indiceContornoNuclear',['../classMorfometria.html#a412132f4f65b0e33b16ba5e663a5a4d0',1,'Morfometria']]],
  ['inicial',['INICIAL',['../livewire_8h.html#a2c889714738551806bfe48393a669e65',1,'livewire.h']]],
  ['inner_5fdelim',['INNER_DELIM',['../namespacePluginProperty_1_1ArrayDelimiter.html#a86dbc427da652e96bc26c06832ece5b6',1,'PluginProperty::ArrayDelimiter']]],
  ['instalarplugin',['instalarPlugin',['../classPluginManager.html#aebd61847ef27f8fac13a15bacd56f16d',1,'PluginManager']]],
  ['intercambiar',['intercambiar',['../classMorfometria.html#add2d16a0b67e4468ae4bacd19dbe5780',1,'Morfometria']]],
  ['interfazcontornos',['interfazContornos',['../classCimahis.html#ac4097c58446be384001bc737e4959c70',1,'Cimahis']]],
  ['interfazforma',['InterfazForma',['../classInterfazForma.html',1,'InterfazForma'],['../classInterfazForma.html#a02f9b710a8c5a756cad2b269b42b72cf',1,'InterfazForma::InterfazForma()']]],
  ['interfazforma_2eh',['interfazforma.h',['../interfazforma_8h.html',1,'']]],
  ['interfazsistema_2eh',['interfazSistema.h',['../interfazSistema_8h.html',1,'']]],
  ['iplimage2qimage',['IplImage2QImage',['../interfazSistema_8h.html#a14432c4d7b162d6e665109a1b88d7b02',1,'interfazSistema.h']]],
  ['iplimagetoqimage',['IplImageToQImage',['../interfazSistema_8h.html#a6e29c56e9bdcd4db41b6b2bc8ddd1731',1,'interfazSistema.h']]],
  ['ischekedarea',['isChekedArea',['../classVentanaConfigParametros.html#a6c0eaa32ea83ff48c990381f4a3c3646',1,'VentanaConfigParametros']]],
  ['ischekedcircularidad',['isChekedCircularidad',['../classVentanaConfigParametros.html#aaa7d8a7b2b17d1a12b4095511f021811',1,'VentanaConfigParametros']]],
  ['ischekeddiametro',['isChekedDiametro',['../classVentanaConfigParametros.html#a05db25228bcbc2ad2891d9ddc0b383c8',1,'VentanaConfigParametros']]],
  ['ischekedejes',['isChekedEjes',['../classVentanaConfigParametros.html#ab996b7c3676985009b5aa8eb0a1600a2',1,'VentanaConfigParametros']]],
  ['ischekedfactorforma',['isChekedFactorForma',['../classVentanaConfigParametros.html#aa1179c12a7933997bad4cbacee2522de',1,'VentanaConfigParametros']]],
  ['ischekedindicecontorno',['isChekedIndiceContorno',['../classVentanaConfigParametros.html#af6fde20b0bc989f01f99bb59374bffa7',1,'VentanaConfigParametros']]],
  ['ischekedmicras',['isChekedMicras',['../classVentanaConfigParametros.html#adac7bcaef0ddae6ee98396f2ef359c4f',1,'VentanaConfigParametros']]],
  ['ischekedperimetro',['isChekedPerimetro',['../classVentanaConfigParametros.html#a66847e4556563b34714a2a5e12e3bc47',1,'VentanaConfigParametros']]],
  ['ischekedra',['isChekedRA',['../classVentanaConfigParametros.html#a12b6852a22d519697c5bd77ade1f72d9',1,'VentanaConfigParametros']]],
  ['ischekedtamanio',['isChekedTamanio',['../classVentanaConfigParametros.html#a946c1637be56c1745adbafb7d6bb9e93',1,'VentanaConfigParametros']]],
  ['isok',['isOK',['../classConfigurarWavelets.html#ac6f619cd0041cbf028dcbf699cf8eb0a',1,'ConfigurarWavelets']]],
  ['ispresetop',['isPresetOp',['../namespaceMorphLib.html#ae73329a6601fc8e4537e655c033ea725',1,'MorphLib']]],
  ['iswavelet',['isWavelet',['../classAreaImagen.html#a2568696ec9029e879164d9cc3222a656',1,'AreaImagen']]],
  ['iterations',['iterations',['../classMorphSettingsDialog.html#a50ac3d27c8f5f46ba14e49904c2ffc2e',1,'MorphSettingsDialog']]]
];
