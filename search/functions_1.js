var searchData=
[
  ['baseimage',['baseImage',['../classSgmDialog.html#a2974557f1a6fa99734afcc3fa3ff09dd',1,'SgmDialog']]],
  ['bordclear',['bordClear',['../namespaceMorphLib.html#aba0ca50e980f31d97b929c1c4d45ffa0',1,'MorphLib']]],
  ['bottomhat',['bottomhat',['../namespaceMorphLib.html#a4f87cfb97d087d2a0808b99841f345fe',1,'MorphLib']]],
  ['boundingrect',['boundingRect',['../classSgmGraphBox.html#a33633e8a3bf0234df85687c7ee52a644',1,'SgmGraphBox::boundingRect()'],['../classSgmGraphContour.html#a0306f71bb6855164be897def2a9dff4c',1,'SgmGraphContour::boundingRect()'],['../classSgmGraphMarker.html#a6e379c4cf8a9d3ba8c6c8014b5ef8d1d',1,'SgmGraphMarker::boundingRect()']]],
  ['box',['box',['../classSgmGraphBox.html#ab79eb6cf169bc33059e5cbeb07d6a085',1,'SgmGraphBox']]],
  ['buscarmenupadre',['buscarMenuPadre',['../classPluginManager.html#a12f33f35ffba9f215233d38e731b8048',1,'PluginManager']]],
  ['buscarpos',['buscarPos',['../classMorfometria.html#a680348536589924d2a37502bc1090b46',1,'Morfometria']]]
];
